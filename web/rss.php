<?php
// édition du début du fichier XML
$xml = '<?xml version="1.0" encoding="iso-8859-1"?><rss version="2.0">';
$xml .= '<channel>'; 
$xml .= '<title>Flux RSS pour Watson</title>';
$xml .= '<link>http://watson.local/rss.xml</link>';
$xml .= '<description>flux RSS realise par le groupe 3 dans le cadre de la troisieme semaine de l\'UE 3DW18 de la LPMI.</description>';
$xml .= '<language>fr</language>';
$xml .= '<category>Creation flux RSS</category>';
$xml .= '<generator>PHP/MySQL</generator>';
$xml .= '<docs>http://www.rssboard.org</docs>';


// Connexion à la base de données
$mysqli = new mysqli("localhost","root","adminsql","watson") ;

// Vérification de la connexion
if (mysqli_connect_errno()) {
    printf("Échec de la connexion : %s\n", mysqli_connect_error());
    exit();
}

$val_sel=$mysqli->query("SELECT * FROM tl_liens ORDER BY tl_liens.lien_id DESC LIMIT 0,15");


while ($row = $val_sel->fetch_row()) {  
	$id=$row[0];
	$titre=$row[2];
	$lien=$row[1];
	$description=$row[3];
 
	$xml .= '<item>';
	$xml .= '<title>'.$titre.'</title>';
	$xml .= '<link>'.$lien.'</link>';
	$xml .= '<description>'.$description.'</description>';
	
	$tag_sel=$mysqli->query("SELECT DISTINCT tl_tags.tag_name FROM tl_liens, tl_tags, tl_tags_liens WHERE tl_tags.tag_id = tl_tags_liens.tag_id AND (tl_tags_liens.lien_id = ".$id.") LIMIT 0,15");
	
	$xml .= '<tags>';
	while ($row = $tag_sel->fetch_row()) {  
		$tag=$row[0];
		$xml .= '<tag>'.$tag.'</tag>';
	}
	$xml .= '</tags>';
	
	$xml .= '</item>';	
}

// édition de la fin du fichier XML
$xml .= '</channel>';
$xml .= '</rss>';

$fp = fopen("rss.xml", 'w+');
fputs($fp, $xml);
fclose($fp);
 
?>